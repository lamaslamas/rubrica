<form method="POST" action="{{route('contacts.new')}}">
@csrf
  <div class="form-group row">
    <label for="inputName" class="col-sm-3 col-form-label"><i class="icon fas fa-user"></i></label>
    <div class="col-sm-8">
      <input type="text" name="name" class="form-control" id="inputName" placeholder="Nome">
    </div>
  </div>
    <div class="form-group row">
    <label for="inputSurname" class="col-sm-3 col-form-label"></label>
    <div class="col-sm-8">
      <input type="text" name="surname" class="form-control" id="inputSurname" placeholder="Cognome">
    </div>
  </div>
    <div class="form-group row">
    <label for="inputMobile" class="col-sm-3 col-form-label"><i class="icon fas fa-phone"></i></label>
    <div class="col-sm-8">
      <input type="text" name="mobile" class="form-control" id="inputMobilePhone" placeholder="Numero cellulare">
    </div>
  </div>
   <div class="form-group row">
    <label for="inputEmail" class="col-sm-3 col-form-label"><i class="icon fas fa-envelope"></i></label>
    <div class="col-sm-8">
      <input type="text" name="email" class="form-control" id="inputEmail" placeholder="Email">
    </div>
  </div>
 <button type="submit" class="btn btn-success" style="    width: 30%;
    margin: 0 auto;
    left: 36%;
    position: relative;">Aggiungi</button>
</form>
