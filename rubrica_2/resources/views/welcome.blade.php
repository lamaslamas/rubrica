@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row ">
        <div class="col-md-4">
            <div class="card left">
                <div class="card-header">
                    Aggiungi un nuovo contatto in rubrica
                </div>
                <div class="card-body">
                    @include('_form')
                </div>
            </div>
        </div>
                <div class="col-md-8 right">
                    <div class="card">
                        <div class="card-header">
                            Rubrica ({{$count}} contatti)
                        </div>
                        <div class="card-body">
                         

                            @include('_contacts')
                    
                    </div>
                    </div>
                </div>
            </div>
        </br>
    </br>
</div>
@endsection
