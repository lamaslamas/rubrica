<style type="text/css">
	
.pt-4, .py-4 {
	padding-top: 0 !important;
}

.col-md-4 {
	padding: 0;
}

.col-md-8 {
	padding: 0;
}

.left {
	background: red;
}

.left>.card-header {
	text-align: center;
	color: white;
	font-size: 20px;
}

.icon {
	color: white;
	font-size: 40px;
	text-align: center;
	margin:0;
}

.right {
	padding: 0 50px;
}

.col-form-label {
	text-align: center;
	padding: 0 !important;
}
.contact-row {
	/*width: 80%;*/
	/*margin: 0 auto;*/
	padding: 20px 10px;
	float: left;
}

.icon-detail {
	font-size:0.9rem;
	color: red;
	padding: 5px;
	border-radius: 20px;
	width: 25px;
	height: 25px;
	text-align: center;
	    box-shadow: 0px 0px 1px #b1b1b1;
}

.contact-row p {
	font-size: 20px;
}

</style>