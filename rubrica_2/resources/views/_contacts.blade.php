@foreach($contacts as $contact)
   <div class="contact-row col-sm-6">

<strong>
    {{$contact['surname']}}
</strong>
<i>
    {{$contact['name']}}
</i>
:  {{$contact['mobile']}}
</div>
<div class="contact-row col-sm-4">
<a href="{{route('contacts.detail',[$loop->index])}}">
    <i class="icon-detail fas fa-chevron-right"></i>
</a>
</div>
<div class="contact-row col-sm-2">
<form action="{{route('contacts.delete',[$loop->index])}}" method="POST" style="display: inline;float: right;">
	<input type="hidden" name="_method" value="DELETE">
	@csrf
	<a class="btn btn-warning" href="{{route('contacts.edit',[$loop->index])}}">Modifica</a>
    <button class="btn btn-danger" type="submit">
        Cancella
    </button>
</form>

</div>
@endforeach
