@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <div class="card">
        <div class="card-header">
          Contatto
        </div>

      </div>
      <form method="POST" action="{{route('contacts.update',[$id])}}">
        @csrf
        @method('PUT')
        <div class="form-group row">
          <label for="inputName" class="col-sm-3 col-form-label"><i class="icon fas fa-user"></i></label>
          <div class="col-sm-8">
            <input type="text" name="name" class="form-control" id="inputName" value="{{$contact['name']}}" placeholder="Nome">
          </div>
        </div>
        <div class="form-group row">
          <label for="inputSurname" class="col-sm-3 col-form-label"></label>
          <div class="col-sm-8">
            <input type="text" name="surname" class="form-control" id="inputSurname" value="{{$contact['surname']}}" placeholder="Cognome">
          </div>
        </div>
        <div class="form-group row">
          <label for="inputMobile" class="col-sm-3 col-form-label"><i class="icon fas fa-phone"></i></label>
          <div class="col-sm-8">
            <input type="text" name="mobile" class="form-control" id="inputMobilePhone" value="{{$contact['mobile']}}" placeholder="Numero cellulare">
          </div>
        </div>
        <div class="form-group row">
          <label for="inputEmail" class="col-sm-3 col-form-label"><i class="icon fas fa-envelope"></i></label>
          <div class="col-sm-8">
            <input type="text" name="email" class="form-control" id="inputEmail" value="{{$contact['email']}}" placeholder="Email">
          </div>
        </div>
        <button type="submit" class="btn btn-warning" style="    width: 30%;
        margin: 0 auto;
        left: 36%;
        position: relative;">Modifica</button>
      </form>


    </div>
    @endsection
