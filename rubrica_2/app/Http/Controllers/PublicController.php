<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PublicController extends Controller
{
    public function index(Request $req)
    {
    	$rubrica= $req->session()->get('rubrica');
        $rubrica = $rubrica ?? [];
        return view('welcome', ['contacts'=>$rubrica, 'count'=>count($rubrica)]);


    }

    public function contactNew(Request $req)
    {

        $name    = $req->input('name');
        $surname = $req->input('surname');
        $mobile  = $req->input('mobile');
        $email  = $req->input('email');

        $contatto = ['name' => $name, 'surname' => $surname, 'mobile' => $mobile, 'email'=>$email];

    

        $req->session()->push('rubrica', $contatto);

        return redirect('/');

    }

    public function contactDetail(Request $req,$id)
    {
    	$rubrica= $req->session()->get('rubrica');
    	$contact=$rubrica[$id];

    	return view('contact', ['contact'=>$contact]);
    }

    public function contactDelete(Request $req, $id)
    {
    	$rubrica= $req->session()->get('rubrica');

    	array_splice($rubrica, $id, 1);


    	$req->session()->put('rubrica', $rubrica);
    	return redirect('/');
    }

    public function contactEdit(Request $req, $id){
        $rubrica= $req->session()->get('rubrica');

        $contact = $rubrica[$id];

        return view('contact-edit', ['contact'=>$contact,
            'id'=>$id]);
    }

    public function contactUpdate(Request $req, $id){
        $name    = $req->input('name');
        $surname = $req->input('surname');
        $mobile  = $req->input('mobile');
        $email  = $req->input('email');

        $contatto = ['name' => $name, 'surname' => $surname, 'mobile' => $mobile, 'email'=>$email];

        $rubrica= $req->session()->get('rubrica');
        $rubrica[$id] = $contatto;
        
        $req->session()->put('rubrica', $rubrica);

        return redirect('/');


    }
}
