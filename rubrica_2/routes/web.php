<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PublicController@index');

Route::get('/contacts/{id}', 'PublicController@contactDetail')->name('contacts.detail')->where('id','[0-9]+');

Route::post('/contacts/new', 'PublicController@contactNew')->name('contacts.new');

Route::delete('/contacts/delete/{id}', 'PublicController@contactDelete')->name('contacts.delete');

Route::get('/contacts/edit/{id}','PublicController@contactEdit')->name('contacts.edit');

Route::put('/contacts/update/{id}','PublicController@contactUpdate')->name('contacts.update');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
